class HeaderComponent extends HTMLElement {
  constructor() {
    super();
  }
  connectedCallback() {
    this.innerHTML = `
    <div class="row" id="header">
      <div id="company-logo-container" class="column hidden">
      </div>
      <div id="bill-header" class="column centered">
        <span id="cnpj"></span>
        <div id="company-address"></div>
        <div>
          Documento Auxiliar da Nota Fiscal de Consumidor Eletrônica
        </div>
      </div>
    </div>
    `;
  }
}
module.exports = HeaderComponent;
