class AuthorizationInfoComponent extends HTMLElement {
  constructor() {
    super();
  }
  connectedCallback() {
    this.innerHTML = `
    <div class="row small-gap">
      <div id="access-info" class="row">
        <div class="row align-center"> <strong>Consulte pela Chave de Acesso em</strong></div>
        <div class="row align-center" id="url"></div>
        <div class="row align-center" id="key"></div>
      </div>
      <!-- Client identification -->
      <div>
          <div class="row align-center centered" id="client-info"></div>
      </div>
      <div class="row" id="bill-info">
        <div class="column">
            <strong id="bill-number"></strong>
        </div>
        <div class="column">
            <strong id="serie"></strong>
        </div>
        <div class="column">
            <strong id="authorization-date"></strong>
        </div>
        <div class="column">
            <strong id="authorization-time"></strong>
        </div>
        <!-- <div class="column">
              Via consumidor
        </div> -->
      </div>
      <!-- Authorization protocol -->
      <div class="row visible" id="authorization-protocol">
        <div class="column centered">
            <span>
                <strong id="authorization-protocol-label"></strong> <span id="authorization-protocol-value"></span>
            </span>
        </div>
      </div>
      <div class="row">
        <div class="column centered" id="authorization-date-time" style="display: inline;"></div>
      </div>
    </div>
      `;
  }
}
module.exports = AuthorizationInfoComponent;
