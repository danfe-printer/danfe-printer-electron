class TotalsComponent extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
    <div id="totals" class="row small-gap">
      <div class="row">
        <div class="column">Qtde. total de itens</div>
        <div class="column align-right" id="total-quantity"></div>
      </div>
      <div class="row">
        <div class="column">Valor total R$</div>
        <div class="column align-right" id="total-value"></div>
      </div>
      <div class="row">
        <div class="column">Desconto R$</div>
        <div class="column align-right" id="discount-value"></div>
      </div>
      <div class="row">
        <div class="column"><strong>Valor a pagar R$</strong></div>
        <div class="column align-right" id="due-value"></div>
      </div>
      <div class="row">
        <div class="column">FORMA PAGAMENTO</div>
        <div class="column align-right">VALOR PAGO R$</div>
      </div>
      <div class="row">
        <div class="column" id="payment-method"></div>
        <div class="column align-right" id="paid-value"></div>
      </div>
      <div class="row">
        <div class="column">Troco R$</div>
        <div class="column align-right" id="change-value"></div>
      </div>
    </div>
      `;
  }
}
module.exports = TotalsComponent;
