'use strict';

// Webcomponents registration (it must be loaded and registred before any other modules).
const headerComponent = require('./components/header');
const totalsComponent = require('./components/totals');
const authorizationInfoComponent = require('./components/authorizationInfo');
customElements.define('header-component', headerComponent);
customElements.define('totals-component', totalsComponent);
customElements.define('authorization-info-component', authorizationInfoComponent);

const { ipcRenderer } = require('electron');
const { render } = require('./utils/render');

// Incoming data from electron main process - DANFE's content from socket client.
ipcRenderer.on('danfe', (_event, args) => {
  render(args);
});

ipcRenderer.on('logo', (_event, filePath) => {
  const imgContainer = document.getElementById('company-logo-container');
  if (filePath) {
    let img = document.getElementById('company-logo');
    if (!img) {
      img = document.createElement('img');
      img.setAttribute('id', 'company-logo');
      img.setAttribute('width', 56);
      img.setAttribute('alt', 'Company logo');
      imgContainer.appendChild(img);
      imgContainer.setAttribute('class', 'column visible');
    }
    img.setAttribute('src', filePath);
  } else {
    document.getElementById('company-logo').remove();
    imgContainer.removeAttribute('class', 'column visible');
  }
});
