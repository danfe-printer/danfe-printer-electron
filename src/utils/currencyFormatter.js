'use strict';

/**
 * @description Formats a number which represents money to Brazilian pattern.
 * @param {String} country The country initials used for currency formatting (the default is the Brazilian pattern **BRL**).
 * @returns {Object} Getter function that formats a number according to the locale and formatting options of this NumberFormat object.
 */
exports.currencyFormatter = (country) => {
  return new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: country || 'BRL',
    minimumFractionDigits: 2,
  });
};
