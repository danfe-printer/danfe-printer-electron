'use strict';
/**
 * @description Formats a UTC timestamp to dd/mm/yyyy pattern.
 * @param {number} date UTC timestamp.
 * @returns {string} Formated date as dd/mm/yyyy.
 */
exports.dateFormatter = (date) => {
    return new Date(date)
        .toISOString()
        .substring(0, 10)
        .split('-')
        .reverse()
        .join('/');
};