'use strict';

/**
 * @description Formats date-time to 00:00:00 pattern.
 * @param {Date} dateTime Date-time as yyyy-mm-ddThh:mm:ss format
 * @returns {string} Time as 00:00:00 pattern.
 */
exports.timeFormatter = (dateTime) => new Date(dateTime).toLocaleString([], { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false });