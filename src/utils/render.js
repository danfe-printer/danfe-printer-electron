'use strict';

const { BillModel } = require('../model/bill');
const { timeFormatter } = require('./timeFormatter');
const { dateFormatter } = require('./dateFormatter');
const { currencyFormatter } = require('./currencyFormatter');

let productList = null;
let contingency = false;
const formater = currencyFormatter('BRL');

// Html elements.
const contingencyMessageHeader = document.getElementById('contingency-message');
const authorizationProtocol = document.getElementById('authorization-protocol');
const authorizationDateTime = document.getElementById(
  'authorization-date-time'
);
const clientInfo = document.getElementById('client-info');

function createColumn() {
  const el = document.createElement('div');
  el.setAttribute('class', 'column');
  return el;
}

function createRow() {
  const el = document.createElement('div');
  el.setAttribute('class', 'row');
  return el;
}

function format(value) {
  return formater.format(value || 0).slice(3);
}

/**
 * @description Renders receipt's content inside the html container.
 * @param {BillModel} receipt receipt content.
 */
exports.render = (receipt) => {
  const bill = Object.assign(BillModel(), receipt);
  contingency = !bill.authorizationProtocol ? true : false;

  // Receipt header
  document.getElementById(
    'cnpj'
  ).innerHTML = `<strong>${bill.header.cnpj}</strong> ${bill.header.socialName}`;

  document.getElementById('company-address').innerText = bill.header.address;

  // Product list (not available for short version).
  if (!bill.shortVersion) {
    document
      .getElementById('product-header')
      .setAttribute('class', 'row visible');
    document
      .getElementById('product-list')
      .setAttribute('class', 'row visible');
    productList = document.getElementById('product-list');
    productList.innerHTML = '';
    bill.products.forEach((product) => {
      let mainRow = createRow();
      mainRow.setAttribute('id', 'product-row');
      let code = createColumn();
      let description = createRow();
      let quantity = createColumn();
      let unity = createColumn();
      const unityValue = createColumn();
      const totalValue = createColumn();
      code.innerHTML = product.code;
      description.innerText = product.description;
      quantity.innerHTML = product.quantity;
      unity.innerText = product.unity;
      unityValue.innerText = format(product.unityValue);
      totalValue.innerText = format(product.totalValue);
      totalValue.setAttribute('class', 'align-right');

      mainRow.appendChild(code);
      mainRow.appendChild(quantity);
      mainRow.appendChild(unity);
      mainRow.appendChild(unityValue);
      mainRow.appendChild(totalValue);
      mainRow.appendChild(description);

      productList.appendChild(mainRow);
    });
  } else {
    document
      .getElementById('product-header')
      .setAttribute('class', 'row hidden');
    document.getElementById('product-list').setAttribute('class', 'row hidden');
  }

  document.getElementById('total-quantity').innerText = bill.totalQuantity;
  document.getElementById('total-value').innerText = format(bill.totalValue);
  document.getElementById('due-value').innerText = format(bill.dueValue);
  document.getElementById('payment-method').innerText = bill.paymentMethod;
  document.getElementById('paid-value').innerText = format(bill.paidValue);
  document.getElementById('change-value').innerText = format(bill.changeValue);
  document.getElementById('discount-value').innerText = format(
    bill.discountValue
  );
  document.getElementById('url').innerText = bill.url;
  document.getElementById('key').innerText = bill.key;
  document.getElementById('serie').innerText = bill.serie;

  document.getElementById('authorization-date').innerText = dateFormatter(
    bill.authorizationTime
  );
  document.getElementById('authorization-time').innerText = timeFormatter(
    bill.authorizationTime
  );

  if (contingency) {
    authorizationProtocol.setAttribute('class', 'row hidden');
    contingencyMessageHeader.setAttribute('class', 'row visible');

    document.getElementById('authorization-protocol-label').innerText = '';
    authorizationDateTime.innerHTML = `
          <h3>EMITIDA EM CONTINGÊNCIA</h3>
          <strong>Pendente de autorização</strong>
    `;
  } else {
    authorizationProtocol.setAttribute('class', 'row visible');
    contingencyMessageHeader.setAttribute('class', 'row hidden');
    document.getElementById('authorization-protocol-label').innerText =
      'Protocolo de autorização';
    authorizationDateTime.innerHTML = `
    <strong>Data de autorização</strong> ${dateFormatter(
      bill.authorizationTime
    )}
    `;
    document.getElementById('authorization-protocol-value').innerText =
      bill.authorizationProtocol;
  }

  // Client info
  if (bill.client) {
    clientInfo.innerText = `CONSUMIDOR - CPF ${bill.client.cpf}${
      bill.client.name ? ` - ${bill.client.name}` : ''
    }${bill.client.address ? ` - ${bill.client.address}` : ''}`;
  } else {
    clientInfo.innerText = 'CONSUMIDOR NÂO IDENTIFICADO';
  }

  document.getElementById(
    'bill-number'
  ).innerText = `NFC-e nº ${bill.receiptNumber}`;

  // Qr code
  const qrCode = document.getElementById('qr-code');
  qrCode.setAttribute('src', bill.qrCode);

  // Custom message (optional)
  document.getElementById('custom-message').innerText =
    bill.customMessage || '';
};
