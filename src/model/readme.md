# Models

📝 This folder contains the main data models which are used across the application. The picture below shows up in a minimalist way how it's designed.

<p align="center">
<img src='./data-model.png' height="400">
</p>

_Figure 1 - A simple overview of how models are organized inside the receipt object._

- `header` Represents the receipt's header model, here comes info about the store such address, name, contact, and so on;
- `client` (optional) Represents the client's model and is shown only if the consumer provides your info in the current sale;
- `product` (optional) Represents the product's model and is shown only in the complete models;
- `receipt` Wraps out all the models and inserts additional info to the structure like receipt's footer message, totals value, QrCode (if provided), authorization info, and so on.
