'use strict';

const { Header } = require('./header');
const { Client } = require('./client');
const { Product } = require('./product');

exports.BillModel = () => ({
  header: Header(),
  products: Product(),
  client: Client(),
  paperWidth: 80,
  printerName: null,
  paymentMethod: null,
  totalValue: 0,
  paidValue: 0,
  dueValue: 0,
  changeValue: 0,
  discountValue: 0,
  totalQuantity: 0,
  key: null,
  serie: null,
  url: null,
  qrCode: null,
  receiptNumber: null,
  authorizationTime: null,
  authorizationProtocol: null,
  customMessage: null,
  shortVersion: false,
  contingency: false,
});
