exports.Product = () => ({
  code: null,
  description: null,
  quantity: 0,
  unity: null,
  unityValue: 0,
  totalValue: 0,
});
