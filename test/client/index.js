let checkList = [];
const { render } = require('../../src/utils/render');
const btnSend = document.getElementById('btn-send');
const socket = new WebSocket('ws://localhost:8008/');
const printerList = document.getElementById('printer-list');
const selectedPrinter = document.getElementById('selected-printer');
let receipt = {
  value: null,
  set payload(value) {
    if (value) {
      if (printerName.value) btnSend.disabled = false;
      this.value = value;
    } else {
      btnSend.disabled = true;
    }
  },
  get payload() {
    return this.value;
  },
};
let printerName = {
  value: null,
  set name(value) {
    if (value) {
      if (receipt.value) btnSend.disabled = false;
      this.value = value;
    } else {
      btnSend.disabled = true;
      this.value = null;
    }
  },
  get name() {
    return this.value;
  },
};

btnSend.disabled = true;

if (printerList.childElementCount === 0) {
  printerList.innerHTML = '<h4>No printers available.</h4>';
}

socket.onopen = () => {
  console.log('WebSocket Client Connected.');
};

socket.onmessage = (message) => {
  const content = JSON.parse(message.data);
  if (content.type === 'printers') {
    printerList.innerHTML = '';
    let li;
    let check;
    checkList = [];
    content.msg.forEach((printerName, index) => {
      li = document.createElement('li');
      li.innerText = printerName;
      check = document.createElement('input');
      check.setAttribute('type', 'checkbox');
      check.setAttribute('id', `printer-${index}`);
      printerList.insertAdjacentElement('beforeend', check);
      printerList.appendChild(li);
      // Printer list click event.
      check.addEventListener('click', (event) => {
        setPrinter(event);
      });
      checkList.push({ check, printerName });
    });
  } else if (content.type === 'warn') {
    alert(content.msg);
  } else {
    console.log(`Received data: ${content.msg}`);
  }
};

window.uploadFile = (event) => {
  const file = event.target.files[0];
  const reader = new FileReader();

  reader.onload = (event) => {
    // Selected printer assignment.
    const aux = JSON.parse(event.target.result);
    aux.printerName = printerName.value;
    receipt.payload = JSON.stringify({ type: 'print', msg: aux });
    render(JSON.parse(event.target.result));
  };

  reader.readAsText(file);
};

window.getPrinters = () => {
  socket.send(JSON.stringify({ type: 'printers', msg: null }));
};

window.sendReceipt = () => {
  if (receipt.value) {
    socket.send(receipt.value);
  }
};
const setPrinter = (event) => {
  if (event.target.checked) {
    checkList.forEach((el) => {
      if (el.check.id !== event.target.id) el.check.disabled = true;
      else {
        el.check.disabled = false;
        printerName.name = el.printerName;
      }
    });
  } else {
    checkList.forEach((el) => {
      el.check.disabled = false;
    });
    btnSend.disabled = true;
    printerName.name = null;
  }
  selectedPrinter.innerText = printerName.name || 'Please, select a printer.';
};
