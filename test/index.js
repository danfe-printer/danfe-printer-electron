'use strict';

const path = require('path');
const assert = require('assert');
const electronBinary = require('electron');
const Application = require('spectron').Application;

describe('Application launch', function () {
  this.timeout(10e3);
  beforeEach(function () {
    console.log('The app is being launched...');
    this.app = new Application({
      path: electronBinary,
      args: [path.join(__dirname, '..')],
    });
    return this.app.start();
  });

  afterEach(function () {
    if (this.app && this.app.isRunning()) {
      return this.app.stop();
    } else {
      console.log('An error occurred on stopping the app.');
    }
  });

  it('Should has one window', function () {
    return this.app.client.getWindowCount().then((count) => {
      assert(count, 1);
    });
  });
  it('Should reveal the browser window', function (done) {
    this.app.browserWindow.show().then(() => {
      done();
    });
  });
  it('Should displays the window title', async function () {
    await this.app.client.pause(4e3);
    await this.app.client.waitUntilWindowLoaded();
    const title = await this.app.client.getTitle();
    assert(title, 'Receipt printer module');
  });
});
