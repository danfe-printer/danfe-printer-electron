'use strict';
const WebSocket = require('ws');
const { app, BrowserWindow, Tray, Menu, dialog } = require('electron');
const isMac = process.platform === 'darwin';

// JSON schema validation lib.
const Ajv = require('ajv');
const ajv = new Ajv();
const schema = require('./src/schema.json');
const validate = ajv.compile(schema);

let win = null;
let tray = null;

function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      // Prevent test issues like: https://github.com/electron-userland/spectron/pull/738 and https://stackoverflow.com/questions/37884130/electron-remote-is-undefined
      enableRemoteModule: true
    }
  });
  win.hide();
  // Recommended only for dev environment. It opens the browser's console.
  // win.webContents.openDevTools();

  // Invisible window printer

  // const win = new BrowserWindow({
  //     transparent: true,
  //     frame: false,
  //     webPreferences: {
  //         nodeIntegration: true
  //     }
  // });

  // win.hide();

  const wss = new WebSocket.Server({ port: 8008 });
  let parsedData;
  win.loadFile('src/index.html').then((_res) => {
    wss.on('connection', function (ws) {
      ws.on('message', (data) => {
        parsedData = JSON.parse(data.toString('utf-8'));
        // Send all available printers.
        if (parsedData.type === 'printers') {
          ws.send(
            JSON.stringify({
              type: 'printers',
              msg: win.webContents.getPrinters().map((printer) => printer.name)
            })
          );
        } else if (parsedData.type === 'print') {
          // JSON schema validation.
          if (!validate(parsedData.msg)) {
            ws.send(
              JSON.stringify({
                type: 'warn',
                msg: 'Invalid JSON schema!',
                details: validate.errors
              })
            );
            return;
          }
          // Send JSON content to the render process;
          win.webContents.send('danfe', parsedData.msg);
          ws.send(
            JSON.stringify({
              type: 'warn',
              msg: 'Receipt received successfully!'
            })
          );
          // Wait for 500 ms, and afterward send the rendered content to the printer.
          // win.webContents.print({ silent: true, deviceName: 'SWEDA SI-300V1' });
          // setTimeout(() => {
          //   console.log('Printing...');
          //   console.log(JSON.parse(parsedData.msg).printerName);
          //   win.webContents.print({
          //     silent: true,
          //     deviceName: 'SWEDA SI-300V1',
          //     margins: { left: 0, right: 0 },
          //     scaleFactor: 1.1,
          //   });
          // }, 500);
        }
      });
      ws.on('error', (err) => {
        console.error(`An error happened: ${err}`);
      });

      ws.on('close', () => {
        console.log('Client disconnected.');
      });

      ws.send(JSON.stringify({ type: 'Handshake', msg: 'Connection established!' }));
    });
  });

  win.on('minimize', function (event) {
    event.preventDefault();
    win.hide();
  });
}

app.whenReady().then(() => {
  createWindow();
  // Initially, the app is opened at system tray.
  tray = new Tray('./assets/icons/64x64.png');
  tray.setToolTip('Printer module');
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Open printer module',
      click: () => win.show()
    },
    {
      label: 'Quit',
      click: () => {
        win.destroy();
        app.quit();
      }
    }
  ]);
  tray.setContextMenu(contextMenu);

  const menu = Menu.buildFromTemplate([
    {
      label: 'File',
      submenu: [
        isMac ? { role: 'close' } : { role: 'quit' },
        {
          label: 'Upload logo',
          async click() {
            const filePath = (
              await dialog.showOpenDialog(win, {
                properties: ['openFile'],
                filters: [
                  {
                    name: 'Image file',
                    extensions: ['jpg', 'png']
                  }
                ]
              })
            ).filePaths[0];

            // Sends the file path to the render process;
            win.webContents.send('logo', filePath || '');
          }
        }
      ]
    }
  ]);
  Menu.setApplicationMenu(menu);
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});
