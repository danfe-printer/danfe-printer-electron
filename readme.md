# Receipt printer module

A cross-platform receipt issuance module made in Electron. This concept introduces a simple approach to connect client apps to the printers, especially the thermal ones widely used in retail stores, bars, shopping malls, and the list goes on.

The most noticeable advantage of performing printer communication by Javascript, HTML, and CSS lies in the fact that is by far simpler than the ESC/POS traditional pattern. 

Moreover, by using Electron, it turns out that several possibilities pop up such as cross-platform development, reusability, speeding up of software development, modern development concepts, and so on.

The project is based on Brazillian DANFE (Documento Auxiliar de Nota Fiscal Eletrônica in Portuguese or Receipt Auxiliar Electronic Document in English) document, however it's entirely possible to modify its structure according to your necessities - in general, what you need for that is adjusting the layout and data pattern to fit your goals.

# Project structure main points

## Layout and styling

The app styling rules are defined inside the ``src/css/main.css`` file.
The main HTML layout is build inside the ``src/index.html`` file. Some other parts are injected inside the main layout via web components which are hosted in ``src/components`` folder.

## Receipt structure render

The render process core - receipt builder, is performed by the `render()` method located in `src/utils/render.js` file. This method takes care of all logic involving the receipt fill-up, such as rules about document width and height, additional messages, etc.

## Application communication 

---

## Product list

This sections is filled up by the product model.

---

### Short version with company's logo

### Full version with company's logo

![A receipt with 80mm long containing the company's logo](80mm-logo-full.png)

_Figure X - A sample illustrating an 80 millimeters receipt on its full version along with the company's logo._

### Short version with company's logo (contingency)

![A receipt with 80mm long containing the company's logo on its short version](80mm-contingency-with-logo-short.png)

_Figure X - A sample illustrating an 80 millimeters contingency receipt on its short version._

## Web client app - playground

The project contains a client module that allows us to select the target printer among all options available in the current host as well as the aimed JSON file.

![The test client module](client.png)

_Figure X - The client module used for testing._

A printer and an appropriate JSON file must be selected before sending that to the receipt service.

## Project setup

Dependencies installation:

```bash
npm install
```

Local tests:
```bash
npm run test
```

Code linter:
```bash
npm run lint
```

To execute the web client app (playground), run in another terminal:
```bash
npm run start-client
```

 Build:
```bash
npm run dist
```
Build config can be found inside package.json file at the build section. For more details about electron builder settings, please take a look at: [Electron Builder Common Configuration](https://www.electron.build/configuration/configuration.html).
